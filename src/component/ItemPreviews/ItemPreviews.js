import React from 'react';
import Item from './Item/Item'
import Graphic from './Graphic/Graphic';
import './ItemPreviews.css';

const ItemPreviews = props => {
    let totalSpent = 0;
    let carSum = 0;
    let foodSum = 0;
    let entertainmentSum = 0;
    let scaleWidth = {};

    let itemsArray = props.items.reduce((acc, el) => {
        acc.push(<Item
            title={el.title}
            price={el.price}
            type={el.type}
            key={el.id}
            remove={() => props.remove(el.id)}
        />);

        totalSpent += parseInt(el.price, 10);

        if(el.type === 'Car') {
            carSum += parseInt(el.price, 10);
        } else if(el.type === 'Entertainment') {
            entertainmentSum += parseInt(el.price, 10);
        } else if(el.type === 'Food') {
            foodSum += parseInt(el.price, 10);
        }

        return acc;
    }, []);

    scaleWidth.car = (carSum / totalSpent) * 100;
    scaleWidth.entetainment = (entertainmentSum / totalSpent) * 100;
    scaleWidth.food = (foodSum / totalSpent) * 100;

    return (
        <div className="ItemPreviews">
            <div className="Items">
                {itemsArray}
            </div>
            <p>TotalSpent: {totalSpent} KGS</p>
            <Graphic scaleWidth={scaleWidth}/>
        </div>
    )
};

export default ItemPreviews;
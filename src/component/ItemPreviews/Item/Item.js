import React from 'react';
import './Item.css';

const Item = props => {
  return (
      <div className="Item">
          <p>{ props.title } ({props.type})</p>
          <span>{ props.price } KGS </span>
          <span onClick={props.remove}>remove</span>
      </div>
  )
};

export default Item;
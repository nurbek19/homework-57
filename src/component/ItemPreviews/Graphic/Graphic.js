import React from 'react';
import './Graphic.css';

const Graphic = props => {
    let scaleWidth = props.scaleWidth;

    return (
        <div className="Graphic">
            <div className="Scales">
                <div className="car" style={{width: scaleWidth.car + '%'}}></div>
                <div className="entertainment" style={{width: scaleWidth.entetainment + '%'}}></div>
                <div className="food" style={{width: scaleWidth.food + '%'}}></div>
            </div>

            <ul className="scale-description">
                <li>
                    <span></span>
                    <p>Car</p>
                </li>

                <li>
                    <span></span>
                    <p>Entertainment</p>
                </li>

                <li>
                    <span></span>
                    <p>Food</p>
                </li>
            </ul>
        </div>
    )
};

export default Graphic;
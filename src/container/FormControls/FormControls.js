import React, {Component} from 'react';
import ItemPreviews from "../../component/ItemPreviews/ItemPreviews";
import './FormControls.css';


class FormControls extends Component {
    state = {
        items: [],
        currentItem: {
            title: '',
            price: '',
            type: 'null'
        }
    };

    addItemTitle = event => {
        let currentItem = this.state.currentItem;

        currentItem.title = event.target.value;

        this.setState({currentItem});
    };

    addItemPrice = event => {
        let currentItem = this.state.currentItem;

        currentItem.price = event.target.value;

        this.setState({currentItem});
    };

    addItemCategory = event => {
        let currentItem = this.state.currentItem;

        currentItem.type = event.target.value;

        this.setState({currentItem})
    };

    addItem = e => {
        e.preventDefault();
        let currentItem = this.state.currentItem;

        if (currentItem.title !== '' && currentItem.price !== '' && currentItem.type !== 'null') {
            const date = new Date();
            currentItem.id = date.getTime();

            const items = [...this.state.items];
            items.unshift(currentItem);

            this.setState({items, currentItem: {title: '', price: '', type: 'null'}});

        } else {
            alert('Please fill forms to add item!');
        }
    };

    removeItem = (id) => {
        const itemIndex = this.state.items.findIndex(item => item.id === id);

        const items = [...this.state.items];
        items.splice(itemIndex, 1);

        this.setState({items});
    };

    render() {
        return (
            <div className="FormControls">
                <form action="#" className="Form">
                    <label htmlFor="item">
                        <input type="text" id="item" placeholder="Item name" value={this.state.currentItem.title}
                               onChange={this.addItemTitle}/>
                    </label>

                    <label htmlFor="cost">
                        <input type="number" id="cost" placeholder="Cost" value={this.state.currentItem.price}
                               onChange={this.addItemPrice}/> KGS
                    </label>

                    <label htmlFor="type">
                        <select name="type" id="type" onChange={this.addItemCategory}
                                value={this.state.currentItem.type}
                        >
                            <option value="null" disabled="true">Choose type</option>
                            <option value="Car">Car</option>
                            <option value="Entertainment">Entertainment</option>
                            <option value="Food">Food</option>
                        </select>
                    </label>

                    <button onClick={this.addItem}>Add</button>
                </form>

                <ItemPreviews items={this.state.items} total={this.state.totalSpent} remove={this.removeItem}/>
            </div>
        );
    }
}

export default FormControls;
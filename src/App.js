import React, { Component } from 'react';
import FormControls from './container/FormControls/FormControls';
import './App.css';

class App extends Component {
  render() {
    return (
      <div className="App">
          <FormControls/>
      </div>
    );
  }
}

export default App;
